import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider } from "./components/auth";

import Home from "./components/Home";
import Login from "./components/login";
import About from "./components/About";
import NavBar from "./components/navBar";
import Books from "./components/Books";
import Profile from "./components/profile";
import NoMatch from "./components/noMatch";
import Book1 from "./components/book1";
import Book2 from "./components/book2";
import Book3 from "./components/book3";
import BookPrice from "./components/bookPrice";
import { RequireAuth } from "./components/requireAuth";

function App() {
  return (
    <div className="App">
      <AuthProvider>
        <BrowserRouter>
          <NavBar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route />
            <Route path="about" element={<About />} />
            <Route path="books" element={<Books />}>
              <Route index element={<Book1 />} />
              <Route path="book1" element={<Book1 />} />
              <Route path="book2" element={<Book2 />} />
              <Route path="book3" element={<Book3 />} />
            </Route>
            <Route path="books/:id" element={<BookPrice />} />
            <Route
              path="/profile"
              element={
                <RequireAuth>
                  <Profile />
                </RequireAuth>
              }
            />
            <Route path="/login" element={<Login />} />
            <Route path="*" element={<NoMatch />} />
          </Routes>
        </BrowserRouter>
      </AuthProvider>
    </div>
  );
}

export default App;
