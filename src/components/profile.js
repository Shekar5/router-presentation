import { Navigate, useNavigate } from "react-router-dom";
import { useAuth } from "./auth";

function Profile() {
    const auth = useAuth();
    const navigate=useNavigate();
    const handleLogout=()=>{
        auth.logout()
        Navigate('/')
    }
    return ( <>Welcome {auth.user} <button onClick={handleLogout}>Logout</button></> );
}

export default Profile;