import React from "react";
import { useParams, useNavigate } from "react-router-dom";

function BookPrice() {
  const navigate = useNavigate();
  let params = useParams();

  let bookId = params.id;
  let price = 0;
  console.log("id of the book", bookId);
  if ((bookId == 1)) {
    price = 30;
  } else if ((bookId == 2)) {
    price = 40;
  } else if ((bookId == 3)) {
    price = 50;
  }

  return (
    <div>
      <button style={{ margin: "20px" }} onClick={() => navigate(-1)}>
        go back
      </button>
      Price of the book{bookId} is {price}Rs
    </div>
  );
}

export default BookPrice;
