import { Link, NavLink } from "react-router-dom";
import { useAuth } from "./auth";

import "./styles.css";
function NavBar() {
    const auth=useAuth();
  return (
    <nav className="navBar">
      <NavLink to="/">Home</NavLink>
      <NavLink to="/about">About</NavLink>
      <NavLink to="/books">Books</NavLink>
      <NavLink to="/profile">Profile</NavLink>

      {!auth.user && <NavLink to="/login">Login</NavLink>}
    </nav>
  );
}

export default NavBar;
